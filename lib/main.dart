import 'package:flutter/material.dart';
import 'package:smooth_scroll/smooth_scroll_example.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SmoothScroll',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SmoothScrollExample(),
    );
  }
}
